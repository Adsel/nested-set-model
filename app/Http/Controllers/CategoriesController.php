<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\NestedSetModel;
use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $structure = NestedSetModel::convToString($categories);
        $names = $structure['names'];
        $levels = $structure['levels'];

        return view('index', compact('names'), compact('levels'));
    }

    public function showAll(){
        $groups = NestedSetModel::getGroups();
        $counter = $groups['counter'];
        $names = $groups['names'];
        return view('groups', compact('names'), compact('counter'));
    }
}
