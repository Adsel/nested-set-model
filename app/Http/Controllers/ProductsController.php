<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\NestedSetModel;

class ProductsController extends Controller
{

    public function index()
    {
        $products = Product::all();
        return view('choose', compact('products'));
    }

    public function show($id)
    {
        $info = NestedSetModel::getBreadcrumbs($id);
        $breadcrumbs = $info['breadcrumbs'];
        $product = $info['product'];

        return view('breadcrumbs', compact('breadcrumbs'), compact('product'));
    }

}
