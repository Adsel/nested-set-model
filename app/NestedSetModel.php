<?php

namespace App;

class NestedSetModel 
{
    public static function convToString($categories){
        $categoriesMin = NestedSetModel::makeFullInfoStructure($categories);
        $count = count($categories);

        return NestedSetModel::drawLines($categoriesMin, $count);
    }

    private static function minifier($categories, $count){
        $categoriesMin;      
        for($i = 0; $i < $count; $i++){
            $categoriesMin[$i]['id'] = $categories[$i]['id'];
            $categoriesMin[$i]['name'] = $categories[$i]['name'];
            $categoriesMin[$i]['from'] = $categories[$i]['lft'];
            $categoriesMin[$i]['to'] = $categories[$i]['rgt'];
        }

        return $categoriesMin;
    }

    private static function isNotLeef($currCat, $parentCat){
        
        if($currCat['from'] - 1 == $parentCat['from']){
            return true;
        }
        else{
            return false;
        }
    }

    
    private static function isBeetwen($currCat, $parentCat){
        if( $currCat['from'] >= $parentCat['from'] && ($currCat['to'] <= $parentCat['to'])){
            return true;
        }
    
        return false; 
    }

    private static function drawLines($array, $count){
        if($count >= 1){
            $strings;
            $levels;
            for($i = 0; $i < $count; $i++){
                $space = "";
                $levels[$i] = $array[$i]['level'];
                for($j = 0; $j < $array[$i]['level']; $j++){
                   $space = $space." . . .";
                } 
                $strings[$i] = $space." | _ _ _ ".$array[$i]['name'];
                
            }

            return array(
                'names' => $strings,
                'levels' => $levels
            );
        }
        else{
            return "pusta tablica";
        }
    }


    public static function getBreadcrumbs($id){
        $product = Product::find($id);
        $categories = Category::all();
        $breadcrumbs;
        $iterator = 0;
        $left = $categories[$product['category_id'] - 1]['lft'];
        $right =  $categories[$product['category_id'] - 1]['rgt'];
        $indexCat = $categories[$product['category_id'] - 1]['id'];
        for($i = 0; $i < $indexCat - 1; $i++){
            if(
                ($left >= $categories[$i]['lft']) &&
                ($right <= $categories[$i]['rgt'])
            ){
                $space = "";
                for($j = 0; $j < $iterator; $j++){
                    $space = $space." . . .";
                }
                $breadcrumbs[$iterator++] = $space." | _ _ _ ".$categories[$i]['name'];
            }
        }

        $space = "";
        for($j = 0; $j < $iterator; $j++){
             $space = $space." . . .";
        }
        $productName = $space." | _ _ _ ".$product['name'];

        $information = array(
            'breadcrumbs' => $breadcrumbs,
            'product' => $productName
        );
        
        return $information;
    }

    public static function getGroups(){
        $categories = NestedSetModel::makeFullInfoStructure(Category::all());
        $products = Product::all();
        //dd(($products));
        for($i = 0; $i < count($categories); $i++){
            $counter = 0;
            for($j = 0; $j < count($products); $j++){
                $productCategory = $categories[$products[$j]['category_id'] - 1];
                if($categories[$i]['from'] <= $productCategory['from'] && $categories[$i]['to'] >= $productCategory['to']){
                    $counter++;
                }
            }
            $categories[$i]['counter'] = $counter;
        }

        return NestedSetModel::groupMinifier($categories);
    }

    private static function makeFullInfoStructure($categories){
        $count = count($categories);
        $categoriesMin = NestedSetModel::minifier($categories, $count);
        $categoriesMin[0]['level'] = 0;
        $categoriesMin[0]['pid'] = 1;
        $last = $categoriesMin[0];

        for($i = 1; $i < $count; $i++){
            $currCat = $categoriesMin[$i];
            $parentCat = $categoriesMin[$i - 1];
            if(NestedSetModel::isNotLeef($currCat, $parentCat)){
                $categoriesMin[$i]['level'] = $parentCat['level'] + 1;
                $categoriesMin[$i]['pid'] = $parentCat['id'];  
                $last = $categoriesMin[$i];
            }
            else{
                $maxLvl = $last['level'];
                $parentCat = $categoriesMin[$last['pid'] - 1];
                $flag = false;
                for($x = $maxLvl; $x > 0; $x--){
                    
                    if(NestedSetModel::isBeetwen($currCat, $parentCat)){
                        $categoriesMin[$i]['level'] = $parentCat['level'] + 1;
                        $categoriesMin[$i]['pid'] = $parentCat['id'];  
                        $last = $categoriesMin[$i];
                        $flag = true;
                    break;
                    }
                    else{
                        $parentCat = $categoriesMin[$parentCat['pid'] - 1];
                    }
                    
                }

                if(!$flag){
                    $categoriesMin[$i]['level'] = 1;
                    $categoriesMin[$i]['pid'] = 1;
                }
            }
        }

        return $categoriesMin;
    }

    private static function groupMinifier($categories){
        $names;
        $counters;
        for($i = 0; $i < count($categories); $i++){
            $names[$i] = $categories[$i]['name'];
            $counters[$i] = $categories[$i]['counter'];
        }

        return array(
            'names' => $names,
            'counter' => $counters
        );
    }
}


?>