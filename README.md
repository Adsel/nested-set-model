# DOKUMENTACJA 

## Spis treści
* [Podejście A - napisanie aplikacji w oparciu o Laravel](#technika-a)
* [Podejście B - sporządzenie kwerend wykonujących założone zadania](#technika-b)


## Technika A 
Napisanie aplikacji w oparciu o Laravel.

* ### Uruchomienie projektu

1. Przygotowanie projektu
    * polecenie: composer install
    * zmień nazwę pliku z .env.example => .env
    * skonfigurować plik .env

2. Uruchomienie serwera dostarczanego przez Laravela

    * polecenie: php artisan serv --port [nr_portu] 
    * lub utworzenie vhost'a.

3. Uruchomienie lokalnej bazy danych.

* ### Instrukcja obsługi dla użytkownika

1. Wybierz z listy interesującą Cię funkcjonalność.

2. Kliknij przycisk 'Przejdź', aby wyświetlić wynik operacji.

3. Jeżeli interesuje Cię breadcrumbs dla konkretnego przedmiotu, to wybierz go z listy przedmiotów, a następnie kliknij 'Wybierz'.

* ### Listingi dla tych funkcji


![Blad_listening1](https://i.imgur.com/VSoEnYp.png)

## Technika B
Sporządzenie kwerend (MySql) wykonujących założone zadania.

* Listiningi dla kwerend

![Blad_listening2](https://i.imgur.com/mV3Tx2y.png)
