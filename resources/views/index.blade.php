@extends('layouts.app')

@section('title', 'Nested Set Model | Pełna struktura drzewa')

@section('content')

    <div class="col-md-6">
        Pełna struktura drzewa: <br>
        @for($i = 0; $i < count($names); $i++)
            {{ $levels[$i] }} {{ $names[$i] }} <br>
        
        @endfor

        <a href="./">Wróć </a> 
    </div>  
@endsection