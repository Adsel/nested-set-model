@extends('layouts.app')

@section('title', 'Nested Set Model')

@section('content')
    <div class="col-md-6">
         <div class="form-group">
            <label for="selectAction">Wybierz akcję</label>
                <select class="form-control" id="selectAction">
                    <option value="index">Wyświetl pełną strukturę drzewa</option>
                    <option value="2">Wyświetl ścieżkę dla konkretnego produktu</option>
                    <option value="3">Informacja o ilości produktów z danej kategorii</option>
                </select>
            <input type="button" class="btn btn-primary" onClick="goTo()" value="Przejdź">
        </div>
    </div>  

    <script>    
    function goTo(){
        let param = document.getElementById("selectAction").value;
        switch(param){
            case "2": window.location.replace("choose-product"); break;
            case "3": window.location.replace("show-groups"); break;
            default: window.location.replace("index");
        }
    }
    </script>
@endsection