@extends('layouts.app')

@section('title', 'Nested Set Model | Policzenie przedmiotów każdej kategorii')

@section('content')

    <div class="col-md-6">
        Liczba przedmiotów w poszczególnych kategoriach: <br>
        <div class="row">
            
            @for($i = 0; $i < count($names); $i++)
                <div class="col-md-6 text-right">
                {{ $names[$i] }}
                </div><div class="col-md-6">
                {{ $counter[$i] }}
                </div>
            @endfor
        </div>
        
        <a href="./">Wróć </a> 
    </div>  
@endsection