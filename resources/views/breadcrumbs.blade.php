@extends('layouts.app')

@section('title', 'Nested Set Model | Breadcrumbs')

@section('content')

    <div class="col-md-6">
        Breadcrumbs: <br>

            @for($i = 0; $i < count($breadcrumbs); $i++)
                {{ $breadcrumbs[$i] }} <br>
            @endfor
            <span style="font-weight: bold; color: #3490dc"> {{ $product }} </span><br>
        
        <a href="./">Wróć </a> 
    </div>  
@endsection