@extends('layouts.app')

@section('title', 'Nested Set Model | Breadcrumbs')

@section('content')

    <div class="col-md-6">
        <div class="form-group">
            <label for="selectProduct">Wybierz produkt</label>
                <select class="form-control" id="selectProduct">
                    @foreach($products as $p)
                        <option value="{{ $p->product_id }}">{{ $p->name }}</option>
                    @endforeach
                </select>
            <input type="button" class="btn btn-primary" onClick="showBreadcrumbs()" value="Wybierz">
        </div>
        

        <a href="./">Wróć </a> 
    </div> 
    <script>
    function showBreadcrumbs(){
        let param = document.getElementById("selectProduct").value;
        console.log(param);
        window.location.replace("product-" + param);
    }  
    </script>
@endsection