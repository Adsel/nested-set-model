<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ URL::asset('/favicons/favicon.ico') }}">
        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">  
            
        <style>
            .footer {
                position: absolute;
                bottom: 0;
                color: #3490dc;
                font-size: 1.2em;
            }

            nav {
                font-size: 1.5em;
                color: #3490dc;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-md-center">
                <nav class="col-md-6 text-left">
                    Nested Set Model
                </nav>
                
            </div>
            <div class="row justify-content-md-center align-items-center">
                @yield('content')
            </div>
            <footer class="row text-center pt-4">
                <div class="col-md-6 footer justify-content-md-center">
                    Marcin Radwan - zadanie rekrutacyjne 2020
                </div>
            </div>
        </div>
    </body>
</html>